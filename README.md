SEX FIELD MANIPULATION CLASS


# Creation
```php
$gender = new Gender();
$gender = new Gender(Gender::MALE);
$gender = new Gender(Gender::FEMALE);
```

# Output
```php
$gender->toString();
$gender->__toString();
strval($gender);
echo $gender;
```

# Compare
```php
$gender->isMale();
$gender->isFemale();
```

# Setters
```php
$gender->setMale();
$gender->setFemale();
$gender->changeGender();
$gender->setGender(Gender::MALE);
$gender->setGender(Gender::FEMALE);
$gender->setGender('girl');
$gender->setGender('man');
```

# Getters
```php
Gender:: getBaseGendersWords();
```