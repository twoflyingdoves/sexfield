<?php
    
    namespace Contaminavit\Field;
    
    /**
     * Class Gender
     * @author  ilya rogulev (Contaminavit)
     * @package Contaminavit\Field
     */
    class Gender
    {
        /**
         * @author ilya rogulev (Contaminavit)
         * @var string
         */
        const STR_VALUE = 'gender';
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var string
         */
        const MALE = 'male';
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var string
         */
        const FEMALE = 'female';
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var integer
         */
        const ERROR_BASE_CODE = 1;
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var string
         */
        const ERROR_BASE_MESSAGE = 'incorrect sex value. given: %s, type: %s';
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var integer
         */
        const ERROR_EXPECTED_STRING_CODE = 2;
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var string
         */
        const ERROR_EXPECTED_STRING_MESSAGE = 'must be a string value. given: %s, type: %s';
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var integer
         */
        const ERROR_EXPECTED_ARRAY_CODE = 3;
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var string
         */
        const ERROR_EXPECTED_ARRAY_MESSAGE = 'must be an array value. given: %s, type: %s';
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var array
         */
        protected $gender_dictionary = array( 'sex', 'gender', 'sexuality' );
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var array
         */
        protected $gender_male_dictionary = array( 'M', 'man', 'male', 'boy', 'mister', 'mr' );
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var array
         */
        protected $gender_female_dictionary = array( 'F', 'woman', 'dame', 'miss', 'female', 'ms', 'girl' );
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @var string
         */
        public $gender = self::MALE;
        
        /**
         * Gender constructor.
         * @param string $gender
         * @author ilya rogulev (Contaminavit)
         */
        public function __construct(string $gender = self::MALE)
        {
            return $this->setGender( $gender );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return array
         */
        public function getGenderDictionary() : array
        {
            return $this->gender_dictionary;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param array $gender_dictionary
         * @return \Contaminavit\Field\Gender
         */
        public function setGenderDictionary(array $gender_dictionary = []) : Gender
        {
            $this->checkCorrectArrayValue( $gender_dictionary );
            
            $this->gender_dictionary = $gender_dictionary;
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param string $dictionary_word
         * @return \Contaminavit\Field\Gender
         */
        public function appendGenderDictionary(string $dictionary_word = '') : Gender
        {
            $this->checkCorrectStringValue( $dictionary_word );
            
            array_push( $this->gender_dictionary, $dictionary_word );
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return array
         */
        public function getGenderMaleDictionary() : array
        {
            return $this->gender_male_dictionary;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param array $gender_male_dictionary
         * @return \Contaminavit\Field\Gender
         */
        public function setGenderMaleDictionary(array $gender_male_dictionary = []) : Gender
        {
            $this->checkCorrectArrayValue( $gender_male_dictionary );
            
            $this->gender_male_dictionary = $gender_male_dictionary;
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param string $dictionary_male_word
         * @return \Contaminavit\Field\Gender
         */
        public function appendGenderMaleDictionary(string $dictionary_male_word = '') : Gender
        {
            $this->checkCorrectStringValue( $dictionary_male_word );
            
            array_push( $this->gender_male_dictionary, $dictionary_male_word );
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return array
         */
        public function getGenderFemaleDictionary() : array
        {
            return $this->gender_female_dictionary;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param array $gender_female_dictionary
         * @return \Contaminavit\Field\Gender
         */
        public function setGenderFemaleDictionary(array $gender_female_dictionary = []) : Gender
        {
            $this->checkCorrectArrayValue( $gender_female_dictionary );
            
            $this->gender_female_dictionary = $gender_female_dictionary;
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param string $dictionary_female_word
         * @return \Contaminavit\Field\Gender
         */
        public function appendGenderFemaleDictionary(string $dictionary_female_word = '') : Gender
        {
            $this->checkCorrectStringValue( $dictionary_female_word );
            
            array_push( $this->gender_female_dictionary, $dictionary_female_word );
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return array
         */
        public function getValidDictionary() : array
        {
            // append base male and female strings and remove doubles
            $valid_dictionary = array_merge(
                self::getBaseGendersWords(),
                $this->getAllAlternativeWords()
            );
            
            $valid_dictionary = array_unique( $valid_dictionary );
            ksort( $valid_dictionary );
            
            return $valid_dictionary;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return array
         */
        public function getAllAlternativeWords() : array
        {
            $alternative_words = array_unique( array_merge(
                $this->gender_male_dictionary,
                $this->gender_female_dictionary
            ) );
            
            ksort( $alternative_words );
            
            return $alternative_words;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return array
         */
        public static function getBaseGendersWords() : array
        {
            return array( self::MALE, self::FEMALE );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return string
         */
        public function getGender() : string
        {
            return $this->gender;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param string $gender
         * @return \Contaminavit\Field\Gender
         */
        public function setGender(string $gender) : Gender
        {
            $this->checkGender( $gender );
            
            $this->gender = $gender;
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return \Contaminavit\Field\Gender
         */
        public function setMale() : Gender
        {
            $this->gender = self::MALE;
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return \Contaminavit\Field\Gender
         */
        public function setFemale() : Gender
        {
            $this->gender = self::FEMALE;
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return \Contaminavit\Field\Gender
         */
        public function changeGender() : Gender
        {
            $this->gender = $this->isMale()
                ? self::FEMALE
                : self::MALE;
            
            return $this;
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return bool
         */
        public function isMale() : bool
        {
            return ( $this->gender == self::MALE );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return bool
         */
        public function isFemale() : bool
        {
            return ( $this->gender == self::FEMALE );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param string $gender_value
         * @return bool
         */
        public static function isValid(string $gender_value) : bool
        {
            return in_array( $gender_value, ( new self() )->getValidDictionary() );
        }
        
        // CONVERT OTHER FORMAT
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return string
         */
        public function __toString() : string
        {
            return $this->toString();
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return string
         */
        public function toString() : string
        {
            return strval( $this->gender );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return array
         */
        public function toArray() : array
        {
            return array( self::STR_VALUE => $this->toString() );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return string
         */
        public function toJson() : string
        {
            return json_encode( $this->toArray() );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @return \stdClass
         */
        public function toObject() : \stdClass
        {
            $obj = new \stdClass();
            
            $gender_string = self::STR_VALUE;
            $obj->$gender_string = $this->gender;
            
            return $obj;
        }
        
        // CHECKERS
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param string $gender
         * @return void
         */
        protected function checkGender(string $gender) : void
        {
            if ( !self::isValid( $gender ) )
            {
                self::throwBaseErrorException( $gender );
            }
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param array $array_value
         * @return void
         */
        protected function checkCorrectArrayValue(array $array_value) : void
        {
            if ( !is_array( $array_value ) || empty( $array_value ) || count( array_filter( array_keys( $array_value ), 'is_string' ) ) > 0 )
            {
                self::throwExpectedArrayValueException( $array_value );
            }
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param string $string_value
         * @return void
         */
        protected function checkCorrectStringValue(string $string_value) : void
        {
            if ( !is_string( $string_value ) || empty( $string_value ) )
            {
                self::throwExpectedStringValueException( $string_value );
            }
        }
        
        // EXCEPTIONS
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param mixed $given_value
         * @return void
         * @throws \InvalidArgumentException
         */
        public static function throwBaseErrorException(mixed $given_value) : void
        {
            $error_message = sprintf( self::ERROR_BASE_MESSAGE, print_r( $given_value, true ), gettype( $given_value ) );
            throw new \InvalidArgumentException( $error_message, self::ERROR_BASE_CODE );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param mixed $expected_array_value
         * @return void
         * @throws \InvalidArgumentException
         */
        public static function throwExpectedArrayValueException(mixed $expected_array_value) : void
        {
            $error_message = sprintf( self::ERROR_EXPECTED_ARRAY_MESSAGE, print_r( $expected_array_value, true ), gettype( $expected_array_value ) );
            throw new  \InvalidArgumentException( $error_message, self::ERROR_EXPECTED_ARRAY_CODE );
        }
        
        /**
         * @author ilya rogulev (Contaminavit)
         * @param mixed $expected_string_value
         * @return void
         * @throws \InvalidArgumentException
         */
        public static function throwExpectedStringValueException(mixed $expected_string_value) : void
        {
            $error_message = sprintf( self::ERROR_EXPECTED_STRING_MESSAGE, print_r( $expected_string_value, true ), gettype( $expected_string_value ) );
            throw new  \InvalidArgumentException( $error_message, self::ERROR_EXPECTED_STRING_CODE );
        }
    }
